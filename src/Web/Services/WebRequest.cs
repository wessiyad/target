using System.Net.Http;
using System.Threading.Tasks;

namespace TargetSample.Services
{
    public class WebRequest : IWebRequest
    {
        private readonly HttpClient _client;

        public WebRequest()
        {
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36");
        }

        public async Task<string> GetProduct(string url)
        {
            var result = string.Empty;
            var response = await _client.GetAsync(url);
            result = await response.Content.ReadAsStringAsync();

            return result;
        }
    }
}