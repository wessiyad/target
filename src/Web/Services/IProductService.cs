using System.Threading.Tasks;
using TargetSample.Model.ViewModel;

namespace TargetSample.Services
{
    public interface IProductService
    {
        Task<ProductViewModel> GetProduct(int id);
        void UpdateProductPricing(int id, CurrentPriceViewModel pricing);
    }
}