namespace TargetSample.Services
{
    public interface IJsonService<T>
    {
        T DeserializeObject(string json);
    }
}
