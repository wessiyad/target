namespace TargetSample.Model
{
    public class Product
    {
        public Product ()
        {
          Item = new Item();
        }

        public Item Item { get; set; }
    }
}