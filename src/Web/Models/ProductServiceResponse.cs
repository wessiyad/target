namespace TargetSample.Model
{
    public class ProductServiceResponse
    {
        public ProductServiceResponse ()
        {
            Product = new Product();          
        }
        
        public Product Product { get; set; }
    }
}