namespace TargetSample.Model.ViewModel
{
    public class CurrentPriceViewModel
    {
        public double Value { get; set; }
        public string CurrencyCode { get; set; }
    }
}