using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace TargetSample.Model
{
    public class ProductPricing
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public int ProductId { get; set; }
        public double Value { get; set; }
        public string CurrencyCode { get; set; }
    }
}