using TargetSample.Model;

namespace TargetSample.Repositories
{
    public interface IProductPricingRepository
    {
        ProductPricing GetPricing(int productId);
        void AddPricing(ProductPricing pricing);
        void UpdatePricing(ProductPricing pricing);
    }
}