using Xunit;
using NSubstitute;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using TargetSample.Services;
using TargetSample.Model.ViewModel;

namespace TargetSample.Tests.Controllers
{
    public class ProductsControllerTests
    {
        private readonly ProductsController _ctrl;
        private readonly IProductService _svc;

        public ProductsControllerTests ()
        {
            _svc = Substitute.For<IProductService>();
            _svc.GetProduct(Arg.Any<int>()).Returns(new ProductViewModel());
            _ctrl = new ProductsController(_svc);          
        }

        [Fact]
        public void GetShouldReturnProductViewModel()
        {
            var result = _ctrl.Get(100);
            Assert.IsType<Task<IActionResult>>(result);
        }
    }
}