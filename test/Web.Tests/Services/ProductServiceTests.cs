using Xunit;
using NSubstitute;

using TargetSample.Model;
using TargetSample.Model.ViewModel;
using TargetSample.Services;
using TargetSample.Repositories;

namespace TargetSample.Tests.Services
{
    public class ProductServiceTests
    {
        private ProductService _svc;
        private IWebRequest _webRequest;
        private IJsonService<ProductServiceResponse> _jsonService;
        private IProductPricingRepository _pricingRepo;

        private string _lastWebRequestUrl = string.Empty;

        public ProductServiceTests()
        {
            _webRequest = Substitute.For<IWebRequest>();
            _webRequest.GetProduct(Arg.Any<string>())
                .Returns(string.Empty)
                .AndDoes(x =>
                {
                    // set the url supplied to the web request to a class var
                    _lastWebRequestUrl = x.Arg<string>();
                });

            _jsonService = Substitute.For<IJsonService<ProductServiceResponse>>();
            _jsonService.DeserializeObject(Arg.Any<string>()).Returns(new ProductServiceResponse());

            _pricingRepo = Substitute.For<IProductPricingRepository>();
            _pricingRepo.GetPricing(Arg.Any<int>()).Returns(new ProductPricing());

            _svc = new ProductService(_webRequest, _jsonService, _pricingRepo);
        }

        // verify we're calling service to get product data
        [Fact]
        public void GetProductShouldMakeRequestToGetProduct()
        {
            var result = _svc.GetProduct(100);
            _webRequest.Received().GetProduct(Arg.Any<string>());
        }

        // verify the url ProductService supplies to WebRequest is for Target redsky api
        [Fact]
        public void GetProductWebRequestCallUrlShouldStartWithRedSky()
        {
            var result = _svc.GetProduct(100);
            _lastWebRequestUrl.StartsWith("http://redsky.target.com");
        }

        // verify we're calling json service to deserialize json data
        [Fact]
        public void GetProductShouldDeserializeProductJson()
        {
            var result = _svc.GetProduct(100);
            _jsonService.Received().DeserializeObject(Arg.Any<string>());
        }

        // verify we're getting pricing from repo
        [Fact]
        public void GetProductShouldGetProductPricing()
        {
            var result = _svc.GetProduct(100);
            _pricingRepo.Received().GetPricing(Arg.Any<int>());
        }

        // verify repo's update pricing is called
        [Fact]
        public void UpdateProductShouldCallUpdatePricing()
        {
            var pricingVm = new CurrentPriceViewModel();
            pricingVm.Value = 9.99;
            pricingVm.CurrencyCode = "USD";

            _svc.UpdateProductPricing(100, pricingVm);
            _pricingRepo.Received().UpdatePricing(Arg.Any<ProductPricing>());
        }
    }
}
