# Target Case Study

## Setup

- Go to [dot.net](http://www.dot.net) to get the .Net Core SDK (or [direct link](https://go.microsoft.com/fwlink/?LinkID=827524))
- After running setup I had version `1.0.0-preview2-003133` you should probably run something similar.
  - check this using the command `dotnet --version`

## Clone the code

- Clone the code
- cd to directory into which you cloned code
- Run the command `dotnet restore`

## Running Sample Web API (GET fuctionality)

- From the root of your cloned code, `cd src\Web`
- Run the command `dotnet run`
- Navigate to [http://localhost:5000/api/products/13860428](http://localhost:5000/api/products/13860428)

## Running PUT functionality (PUT functionality)

- To update the pricing for the request used above make a PUT request to `http://localhost:5000/api/products/13860428`
- Be sure to set `Content-Type` to `application/json`
- Body of the request should be in the following format

```json
{
  "Value": 9.99,
  "CurrencyCode": "USD"
}
```

## Running Unit Tests

- From root directory where you cloned code `dotnet test test\Web.Tests`
  - Alternatively you can cd into the test\Web.Tests directory and simply run `dotnet test`
- You should see results similar to this:

```bash
xUnit.net .NET CLI test runner (64-bit .NET Core win10-x64)
  Discovering: Web.Tests
  Discovered:  Web.Tests
  Starting:    Web.Tests
  Finished:    Web.Tests
=== TEST EXECUTION SUMMARY ===
   Web.Tests  Total: 1, Errors: 0, Failed: 0, Skipped: 0, Time: 0.163s
SUMMARY: Total: 1 targets, Passed: 1, Failed: 0.
```



